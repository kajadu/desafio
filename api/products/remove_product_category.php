
<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoCategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

if (empty($_POST['id_produto_categoria']) || !isset($_POST['id_produto_categoria'])) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Id product's category not found!");
}else{
    $id_produto_categoria = $_POST['id_produto_categoria'];
    $produtoCategoria = new \classes\dao\ProdutoCategoriaDao();
    $produtoCategoria->id_produto_x_categoria = $id_produto_categoria;
    $resultado_delete = $produtoCategoria->excluirProdutoCategoriaById();

    if (!empty($resultado_delete)) {
        $log = new \classes\dao\LogDao();
        $log->acao = "DELETE";        
        $log->tabela = "produtos_x_categorias";
        $log->id_registro = $id_produto_categoria;
        $log->conteudo = json_encode($resultado_delete);
        $log->salvarLog();

        $data["error"] = 0;
        $data["msg"] = utf8_encode("Product has been deleted!");
    }else{

        $data["error"] = 1;
        $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");        
    }
    
}
echo json_encode($data);




