<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoCategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

if (empty($_POST['id_produto']) || !isset($_POST['id_produto'])) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Id product not found!");
}else{
    $id_produto = $_POST['id_produto'];
    $produto = new \classes\dao\ProdutoDao();
    $produto->id_produto = $id_produto;
    $resultado_delete = $produto->excluirProduto();

    if (!empty($resultado_delete)) {
        $log = new \classes\dao\LogDao();
        $log->acao = "DELETE";        
        $log->tabela = "produtos";
        $log->id_registro = $id_produto;
        $log->conteudo = json_encode($resultado_delete);
        $log->salvarLog();

        $produtoCategoria = new \classes\dao\ProdutoCategoriaDao();
        $produtoCategoria->id_produto = $id_produto;
        $produtosCategoriasRemovidos = $produtoCategoria->excluirProdutoCategoriaByIdProduto();

        if (!empty($produtosCategoriasRemovidos)) {
            foreach ($produtosCategoriasRemovidos as $produtoCategoriaRemovido) {
                $log = new \classes\dao\LogDao();
                $log->acao = "DELETE";        
                $log->tabela = "produtos_x_categorias";
                $log->id_registro = $produtoCategoriaRemovido["id_produto_x_categoria"];
                $log->conteudo = json_encode($produtoCategoriaRemovido);
                $log->salvarLog();
            }
        }

        $data["error"] = 0;
        $data["msg"] = utf8_encode("Product has been deleted!");
    }else{

        $data["error"] = 1;
        $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");        
    }
    
}
echo json_encode($data);



