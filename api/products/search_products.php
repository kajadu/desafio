<?php
ini_set("display_errors",1);

require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoDao.php");

$term = filter_var($_REQUEST['term'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_MAGIC_QUOTES, FILTER_NULL_ON_FAILURE);

$produto = new \classes\dao\ProdutoDao();
$produto->nome = $term;
$produtos_localizados = $produto->buscaProdutoAutoComplete();

if (empty($produtos_localizados)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Product not found!");
}else{    
    $data = $produtos_localizados;
}
echo json_encode($data);


