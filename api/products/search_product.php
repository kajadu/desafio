<?php
ini_set("display_errors",1);

require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoDao.php");

$id_produto = $_GET["id_produto"];

$produto = new \classes\dao\ProdutoDao();
$produto->id_produto = $id_produto;
$produto_localizado = $produto->buscaProdutoById();

if (empty($produto_localizado)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Product not found!");
}else{    
    $data = $produto_localizado;
}
echo json_encode($data);


