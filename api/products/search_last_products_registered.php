<?php
ini_set("display_errors",1);

require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoDao.php");

$produto = new \classes\dao\ProdutoDao();
$produtos_localizados = $produto->buscaUltimosProdutosCadastrados();

if (empty($produtos_localizados)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Products not found!");
}else{    
    $data = $produtos_localizados;
}
echo json_encode($data);


