<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoCategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

$error_validacao = 0;
$campos_invalidos = [];

$id_produto = $_POST["id_product"];

if (empty($_POST["name"])) {
    $error_validacao = 1;    
    $campos_invalidos[] = "name";    
}else{    
    $nome = $_POST["name"];
}

if (empty($_POST["sku"])) {
    $error_validacao = 1;    
    $campos_invalidos[] = "sku";
}else{    
    $sku = $_POST["sku"];
}

if (empty($_POST["price"])) {    
    $preco = "0.00";
}else{    
    $preco = $_POST["price"];
}

if (empty($_POST["price"])) {    
    $quantidade = 0;
}else{
    $quantidade = $_POST["quantity"];
}

$descricao = $_POST["description"];

if (isset($_POST["category"])) {
    $categorias = $_POST["category"];
}else{
    $categorias = [];
}
$ativo = 1;

if ($error_validacao > 0) {
    $data["error"] = 1;
    $data["msg"] = "Please fill the obligation fields (*)";
    $data["campos_invalidos"] = $campos_invalidos;    
    echo json_encode($data);
    die();
}else{
    $produto = new \classes\dao\ProdutoDao();
    $produto->id_produto = $id_produto;    
    $produto->nome = $nome;
    $produto->sku = $sku;
    $produto->preco = $preco;
    $produto->descricao = $descricao;
    $produto->quantidade = $quantidade;
    $produto->ativo = $ativo;    
    $result_update = $produto->updateProduto();

    if ($result_update["error"] > 0) {
        $data["error"] = 1;
        $data["msg"] = $result["msg"];
        echo json_encode($data);
        die();
    }else{
        if (empty($result_update["id_produto"])) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");            
        }else{

            $log = new \classes\dao\LogDao();
            $log->acao = "UPDATE";        
            $log->tabela = "produtos";
            $log->id_registro = $id_produto;
            $log->conteudo = json_encode($result_update);
            $log->salvarLog();


            $produtoCategoria = new \classes\dao\ProdutoCategoriaDao();
            $produtoCategoria->id_produto = $id_produto;
            $produtosCategoriasRemovidos = $produtoCategoria->excluirProdutoCategoriaByIdProduto();

            if (!empty($produtosCategoriasRemovidos)) {
                foreach ($produtosCategoriasRemovidos as $produtoCategoriaRemovido) {
                    $log = new \classes\dao\LogDao();
                    $log->acao = "DELETE";        
                    $log->tabela = "produtos_x_categorias";
                    $log->id_registro = $produtoCategoriaRemovido["id_produto_x_categoria"];
                    $log->conteudo = json_encode($produtoCategoriaRemovido);
                    $log->salvarLog();
                }
            }
            
            $produtoCategoria->id_produto = $result_update["id_produto"];            
            $errorSalvarCategoria = 0;
            foreach ($categorias as $id_categoria) {
                $produtoCategoria->id_categoria = $id_categoria;                
                $result = $produtoCategoria->salvarProdutoCategoria();
                if (empty($result)) {
                    $errorSalvarCategoria = 1;
                }
            }
    
            if ($errorSalvarCategoria) {
                $data["error"] = 1;
                $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");
            } else {            
                $data["error"] = 0;
                $data["msg"] = utf8_encode("Product updated sucessfully!");
            }        
        }
    }    

}  

echo json_encode($data);



