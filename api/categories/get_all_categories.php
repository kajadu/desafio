<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/CategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

$categoria = new \classes\dao\CategoriaDao();
$categorias_localizadas = $categoria->buscaCategorias();

if (empty($categorias_localizadas)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Categories not found!");
}else{    
    $data = $categorias_localizadas;
}
echo json_encode($data);
