<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/CategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

$error_validacao = 0;
$campos_invalidos = [];

$id_categoria = $_POST["id_category"];

if (empty($_POST["category_name"])) {
    $error_validacao = 1;    
    $campos_invalidos[] = "category_name";    
}else{    
    $nome = $_POST["category_name"];
}

if (empty($_POST["category_code"])) {
    $error_validacao = 1;    
    $campos_invalidos[] = "category_code";
}else{    
    $codigo = $_POST["category_code"];
}

$ativo = 1;

if ($error_validacao > 0) {
    $data["error"] = 1;
    $data["msg"] = "Please fill the obligation fields (*)";
    $data["campos_invalidos"] = $campos_invalidos;    
    echo json_encode($data);
    die();
}else{
    $categoria = new \classes\dao\CategoriaDao();
    $categoria->id_categoria = $id_categoria;
    $categoria->nome = $nome;
    $categoria->codigo = $codigo;
    $categoria->ativo = $ativo;    
    $result_update = $categoria->updateCategoria();

    if ($result_update["error"] > 0) {
        $data["error"] = 1;
        $data["msg"] = $result_update["msg"];
        echo json_encode($data);
        die();
    }else{

        $log = new \classes\dao\LogDao();
        $log->acao = "UPDATE";        
        $log->tabela = "categorias";
        $log->id_registro = $id_categoria;
        $log->conteudo = json_encode($result_update);
        $log->salvarLog();

        if (empty($result_update["id_categoria"])) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");            
        }else{
               
            $data["error"] = 0;
            $data["msg"] = utf8_encode("Category updated sucessfully!");
           
        }
    }    

}  

echo json_encode($data);



