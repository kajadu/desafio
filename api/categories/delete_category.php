<?php
ini_set("display_errors",1);
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/CategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/ProdutoCategoriaDao.php");
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/LogDao.php");

if (empty($_POST['id_categoria']) || !isset($_POST['id_categoria'])) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Id category not found!");
}else{
    $id_categoria = $_POST['id_categoria'];
    $categoria = new \classes\dao\CategoriaDao();
    $categoria->id_categoria = $id_categoria;
    $resultado_delete = $categoria->excluirCategoria();

    if (!empty($resultado_delete)) {
        $log = new \classes\dao\LogDao();
        $log->acao = "DELETE";        
        $log->tabela = "categorias";
        $log->id_registro = $id_categoria;
        $log->conteudo = json_encode($resultado_delete);
        $log->salvarLog();

        $produtoCategoria = new \classes\dao\ProdutoCategoriaDao();
        $produtoCategoria->id_categoria = $id_categoria;
        $produtosCategoriasRemovidos = $produtoCategoria->excluirProdutoCategoriaByIdCategoria();

        if (!empty($produtosCategoriasRemovidos)) {
            foreach ($produtosCategoriasRemovidos as $produtoCategoriaRemovido) {
                $log = new \classes\dao\LogDao();
                $log->acao = "DELETE";
                $log->tabela = "produtos_x_categorias";
                $log->id_registro = $produtoCategoriaRemovido["id_produto_x_categoria"];
                $log->conteudo = json_encode($produtoCategoriaRemovido);
                $log->salvarLog();
            }
        }

        $data["error"] = 0;
        $data["msg"] = utf8_encode("Category has been deleted!");
        
    }else{

        $data["error"] = 1;
        $data["msg"] = utf8_encode("Something goes wrong, please report this problem to IT department!");        
    }
    
}
echo json_encode($data);



