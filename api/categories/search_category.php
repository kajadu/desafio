<?php
ini_set("display_errors",1);

require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/CategoriaDao.php");

$id_categoria = $_GET["id_categoria"];

$categoria = new \classes\dao\CategoriaDao();
$categoria->id_categoria = $id_categoria;
$categorias_localizadas = $categoria->buscaCategoriaById();

if (empty($categorias_localizadas)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Category not found!");
}else{    
    $data = $categorias_localizadas;
}
echo json_encode($data);


