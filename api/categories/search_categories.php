<?php
ini_set("display_errors",1);

require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/dao/CategoriaDao.php");

$term = filter_var($_REQUEST['term'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_MAGIC_QUOTES, FILTER_NULL_ON_FAILURE);

$categoria = new \classes\dao\CategoriaDao();
$categoria->nome = $term;
$categorias_localizadas = $categoria->buscaCategoriaAutoComplete();

if (empty($categorias_localizadas)) {
    $data["error"] = 1;
    $data["msg"] = utf8_encode("Category not found!");
}else{    
    $data = $categorias_localizadas;
}
echo json_encode($data);


