$(function(){

    $.ajax({
        type: "GET",
        url: "/desafio/api/products/search_last_products_registered.php",
        data: {},
        dataType: "json",
        success: function (response) {

            var html = ''+
            '<table class="data-grid">'+
                '<tr class="data-row">'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Name</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">SKU</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Price</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Quantity</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Categories</span>'+
                    '</th>'+
            
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Actions</span>'+
                    '</th>'+
                '</tr>';
                
                response.forEach(produto => {        
                    var categorias = produto.categorias;

                    html += '<tr class="data-row">'+
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">'+produto.nome+'</span>'+
                        '</td>'+
                        
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">'+produto.sku+'</span>'+
                        '</td>'+
                
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">R$ '+produto.preco+'</span>'+
                        '</td>'+
                
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">'+produto.quantidade+'</span>'+
                        '</td>'+
                
                        '<td class="data-grid-td">';                        
                            categorias.forEach(categoria => {
                                html += '<span class="data-grid-cell-content badge bg-secondary mt-1">'+categoria.categoria_nome+' <i id_product_category="'+categoria.id_produto_x_categoria+'" class="fas fa-times-circle btn text-white remove-product-category"></i></span><br/>';
                            });                        
                        html += '</td>'+
                        
                        '<td class="data-grid-td">'+
                            '<div class="actions">'+
                                '<button type="button" id_product="'+produto.id_produto+'" class="edit-product btn btn-warning" title="Edit product"><i class="far fa-edit"></i></button>'+
                                '<button type="button" id_product="'+produto.id_produto+'" class="delete-product btn btn-danger ml-2" title="Delete product"><i class="fas fa-trash-alt"></i></button>'+
                            '</div>'+
                        '</td>'+
                    '</tr>';
                });

            html += '</table>';

            $("#result_product").html(html);
        }
    });

    $("#name_product").autocomplete({
        
        source: "/desafio/api/products/search_products.php",
        dataType: "json",
        minLength: 3,

        select: function(event, ui)
        {
            var categorias = ui.item.categorias;
            
            var html = ''+
            '<table class="data-grid">'+
                '<tr class="data-row">'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Name</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">SKU</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Price</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Quantity</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Categories</span>'+
                    '</th>'+
            
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Actions</span>'+
                    '</th>'+
                '</tr>'+            
                '<tr class="data-row">'+
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">'+ui.item.nome+'</span>'+
                    '</td>'+
                    
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">'+ui.item.sku+'</span>'+
                    '</td>'+
            
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">R$ '+ui.item.preco+'</span>'+
                    '</td>'+
            
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">'+ui.item.quantidade+'</span>'+
                    '</td>'+
            
                    '<td class="data-grid-td">';                        
                        categorias.forEach(categoria => {
                            html += '<span class="data-grid-cell-content badge bg-secondary">'+categoria.categoria_nome+' <i id_product_category="'+categoria.id_produto_x_categoria+'" class="fas fa-times-circle btn text-white remove-product-category"></i></span><br/>';
                        });                        
                    html += '</td>'+
                    
                    '<td class="data-grid-td">'+
                        '<div class="actions">'+
                            '<button type="button" id_product="'+ui.item.id_produto+'" class="edit-product btn btn-warning" title="Edit product"><i class="far fa-edit"></i></button>'+
                            '<button type="button" id_product="'+ui.item.id_produto+'" class="delete-product btn btn-danger ml-2" title="Delete product"><i class="fas fa-trash-alt"></i></button>'+
                        '</div>'+
                    '</td>'+
                '</tr>'+
            '</table>';

            $("#result_product").html(html);
        }
    });

    $(document).on("click", ".delete-product", function () {
        var id_produto = $(this).attr("id_product");
        
        $("#result_manage_products").hide("fade");
        $.ajax({
            type: "POST",
            url: "/desafio/api/products/delete_product.php",
            data: {"id_produto": id_produto},
            dataType: "json",
            success: function (response) {
                if (response.error > 0) {
                    $("#result_manage_products").html('<div class="alert alert-danger">'+response.msg+'</div>');
                    $("#result_manage_products").show("fade");
                } else {
                    $("#result_manage_products").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#result_manage_products").show("fade");

                    location.reload();
                }
            }
        });
    });

    $(document).on("click", ".edit-product", function () {
        var id_produto = $(this).attr("id_product");
        window.location.href = "/desafio/assets/editProduct.php?id="+id_produto;
    });

    $(document).on("click", ".remove-product-category", function () {
        var id_produto_categoria = $(this).attr("id_product_category");

        $("#result_manage_products").hide("fade");
        $.ajax({
            type: "POST",
            url: "/desafio/api/products/remove_product_category.php",
            data: {"id_produto_categoria": id_produto_categoria},
            dataType: "json",
            success: function (response) {
                if (response.error > 0) {
                    $("#result_manage_products").html('<div class="alert alert-danger">'+response.msg+'</div>');
                    $("#result_manage_products").show("fade");
                } else {
                    $("#result_manage_products").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#result_manage_products").show("fade");

                    location.reload();
                }
            }
        });
    });
    
});