$(function(){

    $.ajax({
        type: "GET",
        url: "/desafio/api/products/search_last_products_registered.php",
        data: {},
        dataType: "json",
        success: function (response) {
            var html = ''+
            '<div class="row">';
                response.forEach(produto => {
                    console.log(produto);
                    if (produto.quantidade == 0) {
                        stock = 'Out of stock';
                    }else{
                        stock = produto.quantidade+' Available';
                    }
                    html += '<div class="product-info col-3 mt-5">'+
                        '<div class="product-name"><span><a href="editProduct.php?id='+produto.id_produto+'">'+produto.nome+'</a></span></div>'+
                        '<div class="product-price"><span class="special-price">'+stock+'</span> <span>R$ '+produto.preco+'</span></div>'+
                    '</div>';                    
                });                
            html += '</div>';

            $("#product_list").html(html);
        }
    });
    
});