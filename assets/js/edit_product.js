$(function(){
    
    $.ajax({
        type: "GET",
        url: "/desafio/api/categories/get_all_categories.php",
        data: {},
        dataType: "json",
        success: function (response) {
            var categorias = response;
            categorias.forEach(categoria => {
                $("#category").append('<option id="'+categoria.id_categoria+'" value="'+categoria.id_categoria+'">'+categoria.nome+'</option>')
            });
        }
    });

    $.ajax({
        type: "GET",
        url: "/desafio/api/products/search_product.php",
        data: {"id_produto": $("#id_product").val()},
        dataType: "json",
        success: function (response) {

            $("#sku").val(response.sku);
            $("#name").val(response.nome);
            $("#price").val(response.preco);
            $("#quantity").val(response.quantidade);

            if (response.categorias.length > 0) {
                response.categorias.forEach(categoria => {
                    option = $("#category").find("option#"+categoria.id_categoria);
                    option.prop("selected",true);
                });
            }

            $("#description").val(response.descricao);
        }
    });

    $('#price').priceFormat({
        prefix: '',
        thousandsSeparator: '',
    });

    $("input").focus(function () { 
        $(this).removeClass("border border-danger");
        $("#result_save").hide("fade");
    });
    
    $("#save_product").click(function () { 
        $.ajax({
            type: "POST",
            url: "/desafio/api/products/update_product.php",
            data: $("#form_product").serialize(),
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.error > 0) {
                    campos_invalidos = response.campos_invalidos;
                    
                    if (campos_invalidos != undefined) {
                        campos_invalidos.forEach(campo => {
                            $("#"+campo).addClass("border border-danger");
                        });
                    }
                    
                    $("#result_save").html('<div class="alert alert-danger">'+response.msg+'</div>');
                    $("#result_save").show("fade");
                } else {
                    $("#result_save").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#result_save").show("fade");
                }
            }
        });

    });

    
});