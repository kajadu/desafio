$(function(){

    $("input").focus(function () { 
        $(this).removeClass("border border-danger");
        $("#result_save").hide("fade");
    });
    
    $("#save_category").click(function () {
        $.ajax({
            type: "POST",
            url: "/desafio/api/categories/add_category.php",
            data: $("#form_category").serialize(),
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.error > 0) {
                    campos_invalidos = response.campos_invalidos;
                    
                    if (campos_invalidos != undefined) {
                        campos_invalidos.forEach(campo => {
                            $("#"+campo).addClass("border border-danger");
                        });
                    }
                    
                    $("#result_save").html('<div class="alert alert-danger">'+response.msg+'</div>');
                    $("#result_save").show("fade");
                } else {
                    $("#result_save").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#result_save").show("fade");
                }
            }
        });

    });

    
});