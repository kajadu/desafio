$(function(){
    
    $.ajax({
        type: "GET",
        url: "/desafio/api/categories/get_all_categories.php",
        data: {},
        dataType: "json",
        success: function (response) {
            
            var html = ''+
            '<table class="data-grid">'+
                '<tr class="data-row">'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Name</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Code</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Actions</span>'+
                    '</th>'+
                '</tr>';
                response.forEach(categorie => {
                    html += '<tr class="data-row">'+
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">'+categorie.nome+'</span>'+
                        '</td>'+
                    
                        '<td class="data-grid-td">'+
                            '<span class="data-grid-cell-content">'+categorie.codigo+'</span>'+
                        '</td>'+
                        
                        '<td class="data-grid-td">'+
                            '<div class="actions">'+
                                '<button type="button" id_category="'+categorie.id_categoria+'" class="edit-category btn btn-warning" title="Edit category"><i class="far fa-edit"></i></button>'+
                                '<button type="button" id_category="'+categorie.id_categoria+'" class="delete-category btn btn-danger ml-2" title="Delete category"><i class="fas fa-trash-alt"></i></button>'+
                            '</div>'+
                        '</td>'+
                    '</tr>';
                });
            html += '</table>';

            $("#result_categories").html(html);
        }
    });

    $("#name_category").autocomplete({
        
        source: "/desafio/api/categories/search_categories.php",
        dataType: "json",
        minLength: 2,

        select: function(event, ui)
        {   
            var html = ''+
            '<table class="data-grid">'+
                '<tr class="data-row">'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Name</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Code</span>'+
                    '</th>'+
                    '<th class="data-grid-th">'+
                        '<span class="data-grid-cell-content">Actions</span>'+
                    '</th>'+
                '</tr>'+
                
                '<tr class="data-row">'+
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">'+ui.item.nome+'</span>'+
                    '</td>'+
                
                    '<td class="data-grid-td">'+
                        '<span class="data-grid-cell-content">'+ui.item.codigo+'</span>'+
                    '</td>'+
                    
                    '<td class="data-grid-td">'+
                        '<div class="actions">'+
                            '<button type="button" id_category="'+ui.item.id_categoria+'" class="edit-category btn btn-warning" title="Edit category"><i class="far fa-edit"></i></button>'+
                            '<button type="button" id_category="'+ui.item.id_categoria+'" class="delete-category btn btn-danger ml-2" title="Delete category"><i class="fas fa-trash-alt"></i></button>'+
                        '</div>'+
                    '</td>'+
                '</tr>'+
                
            '</table>';

            $("#result_categories").html(html);
        }
    });

    $(document).on("click", ".edit-category", function () {
        var id_categoria = $(this).attr("id_category");
        window.location.href = "/desafio/assets/editCategory.php?id="+id_categoria;
    });

    $(document).on("click", ".delete-category", function () {
        var id_categoria = $(this).attr("id_category");
        $("#result_manage_categories").hide("fade");
        
        $.ajax({
            type: "POST",
            url: "/desafio/api/categories/delete_category.php",
            data: {"id_categoria": id_categoria},
            dataType: "json",
            success: function (response) {
                if (response.error > 0) {
                    $("#result_manage_categories").html('<div class="alert alert-danger">'+response.msg+'</div>');
                    $("#result_manage_categories").show("fade");
                } else {
                    $("#result_manage_categories").html('<div class="alert alert-success">'+response.msg+'</div>');
                    $("#result_manage_categories").show("fade");

                    location.reload();
                }
            }
        });
    });
    
    
});