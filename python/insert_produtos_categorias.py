#!/usr/bin/env python
# coding: utf-8
from mysql.connector.connection import MySQLConnection
import pandas as pd
import mysql.connector
from datetime import date

def getCategoryCodeSequencial(mycursor):
    sqlCategoria = "SELECT codigo FROM categorias WHERE ativo = 1 ORDER BY id_categoria DESC LIMIT 1"
    mycursor.execute(sqlCategoria)
    codigoCategoria = mycursor.fetchone()
    codigo = codigoCategoria
    
    if codigo is None:
        sequencial = "CAT-00001"
    else:
        list_codigo = str(codigo[0]).split("-")
        sequencial = int(list_codigo[1])+1
        sequencial = str(sequencial).rjust(5, '0')
        sequencial = "CAT-"+str(sequencial)
        
    return sequencial

    

data_atual = date.today()

mydb = mysql.connector.connect(
        host="31.220.104.219",
        user="u547467671_lgjump",
        passwd="pWOpCZ|5",
        database="u547467671_webjump"
)

mycursor = mydb.cursor()

path_folder = "import.csv"
sheet_idx = 1
produtos = pd.read_csv(path_folder,delimiter=";")

lista_produtos = produtos.values.tolist()

for produto in lista_produtos:
    nome = str(produto[0])
    sku = str(produto[1])
    descricao = str(produto[2])
    quantidade = str(produto[3])
    preco = str(produto[4])
    categorias = str(produto[5])

    sqlProdutos = "SELECT * FROM produtos WHERE sku = '"+str(sku)+"'"
    mycursor.execute(sqlProdutos)
    produtoResult = mycursor.fetchone()

    if produtoResult is None:
        insertProduto = """INSERT INTO produtos
                            SET                                        
                                nome = '"""+nome+"""',
                                sku = '"""+sku+"""',
                                preco = """+preco+""",
                                descricao = '"""+descricao+"""',
                                quantidade = """+quantidade+""",
                                ativo = 1
        """
        mycursor.execute(insertProduto)
        idProduto = mycursor.lastrowid
    else:
        idProduto = produtoResult[0]
    
    if categorias != "" or categorias == "nan":
        list_categorias = categorias.split("|")
        for categoria in list_categorias:
            sqlCategorias = "SELECT * FROM categorias WHERE nome LIKE '"+str(categoria)+"'"
            mycursor.execute(sqlCategorias)
            categoriaResult = mycursor.fetchone()
            
            if categoriaResult is None:
                codigo = getCategoryCodeSequencial(mycursor)            
                insertCategoria = """INSERT INTO categorias
                                    SET                                        
                                        nome = '"""+categoria+"""',
                                        codigo = '"""+codigo+"""',
                                        ativo = 1
                """        
                mycursor.execute(insertCategoria)
                idCategoria = mycursor.lastrowid
            else:
                idCategoria = categoriaResult[0]

            sqlProdutosCategorias = "SELECT * FROM produtos_x_categorias WHERE id_produto = "+str(idProduto)+" AND id_categoria = "+str(idCategoria)
            mycursor.execute(sqlProdutosCategorias)
            produtoCategoriaResult = mycursor.fetchone()

            if produtoCategoriaResult is None:
                insertCategoria = """INSERT INTO produtos_x_categorias
                                    SET                                        
                                        id_produto = """+str(idProduto)+""",
                                        id_categoria = """+str(idCategoria)+""",
                                        ativo = 1
                """
                mycursor.execute(insertCategoria)
print("done")
    
    


    
    
    