<?php
namespace classes\abstract_class;

abstract class Entity {
    
     protected $conn = "";

     public function __construct() {
          $this->conn = new \PDO("mysql:host=31.220.104.219;dbname=u547467671_webjump", "u547467671_lgjump", "pWOpCZ|5");
	}
 
     /**
     * Metodo magico Retorna o conteudo do valor setado
     *
     * @param Mixed $valor da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __get($valor)
     {         
          
          return $this->$valor;
     }

     /**
     * Metodo magico Adiciona o conteudo a propriedade da classe
     *
     * @param Mixed $propriedade da propriedade da classe
     * @param Mixed $valor       da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __set($propriedade,$valor)
     {
          $this->$propriedade = $valor;
          
     }   
}
