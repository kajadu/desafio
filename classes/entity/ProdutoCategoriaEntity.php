<?php
namespace classes\entity;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/abstract_class/Entity.php");

class ProdutoCategoriaEntity extends \classes\abstract_class\Entity
{   
    public function __construct() {
        parent::__construct();
    }
    
    protected $id_produto_x_categoria;
	protected $id_produto;
	protected $id_categoria;
    protected $ativo = 1;
    protected $data_cadastro;
}
