<?php
namespace classes\entity;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/abstract_class/Entity.php");

class CategoriaEntity extends \classes\abstract_class\Entity
{   
    public function __construct() {
        parent::__construct();
    }
    
    protected $id_categoria;
	protected $codigo;
	protected $nome;
	protected $ativo = 1;
	protected $data_cadastro;
    
}
