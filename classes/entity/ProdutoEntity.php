<?php
namespace classes\entity;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/abstract_class/Entity.php");

class ProdutoEntity extends \classes\abstract_class\Entity
{   
    public function __construct() {
        parent::__construct();
    }
    
    protected $id_produto;
    protected $nome;
    protected $sku;
    protected $preco;
    protected $descricao;
    protected $quantidade;
    protected $ativo = 1;
    protected $data_cadastro;
}
