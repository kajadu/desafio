<?php
namespace classes\dao;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/entity/LogEntity.php");

class LogDao extends \classes\entity\LogEntity {

    public function __construct() {
        parent::__construct();
    }

    public function salvarLog()
    {
        $insertLog = "INSERT INTO logs
                    SET
                    acao = '{$this->acao}',
	                tabela = '{$this->tabela}',
	                id_registro = {$this->id_registro},
                    conteudo = '{$this->conteudo}'
        ";

        $stmt = $this->conn->prepare($insertLog);
        $result = $stmt->execute();
        
        return $result;
        
    }

}