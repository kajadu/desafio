<?php
namespace classes\dao;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/entity/ProdutoEntity.php");

class ProdutoDao extends \classes\entity\ProdutoEntity {

    public function __construct() {
        parent::__construct();
    }
    
    public function buscaProdutoById()
    {
        $sql = "SELECT * FROM produtos WHERE ativo = 1 AND id_produto LIKE '%{$this->id_produto}%'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $produto = $stmt->fetch(\PDO::FETCH_ASSOC);

        
        $produto["nome"] = utf8_encode($produto["nome"]);
        $produto["descricao"] = utf8_encode($produto["descricao"]);

        $sqlProdutosCategorias = "SELECT
            produtos_x_categorias.id_produto_x_categoria,
            categorias.id_categoria,
            categorias.nome AS categoria_nome
        FROM produtos_x_categorias
        LEFT JOIN categorias ON categorias.id_categoria = produtos_x_categorias.id_categoria
        WHERE
            produtos_x_categorias.id_produto = {$produto['id_produto']} AND
            produtos_x_categorias.ativo = 1 AND 
            categorias.ativo = 1
            ";

        $stmt = $this->conn->prepare($sqlProdutosCategorias);
        $stmt->execute();
        $produtoCategorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        if (empty($produtoCategorias)) {
            $produto["categorias"] = [];
        }else{
            $produto["categorias"] = $produtoCategorias;
        }

        return $produto;
    }

    public function buscaProdutoAutoComplete()
    {
        $sql = "SELECT * FROM produtos WHERE ativo = 1 AND nome LIKE '%{$this->nome}%'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $produto) {
            $resultado[$key]["nome"] = utf8_encode($produto["nome"]);
            $resultado[$key]["descricao"] = utf8_encode($produto["descricao"]);
            $resultado[$key]["label"] = utf8_encode($produto["nome"]." | ".$produto["sku"]);
            $resultado[$key]["id"] = $produto["id_produto"];

            $sqlProdutosCategorias = "SELECT
                produtos_x_categorias.id_produto_x_categoria,
                categorias.nome AS categoria_nome
            FROM produtos_x_categorias
            LEFT JOIN categorias ON categorias.id_categoria = produtos_x_categorias.id_categoria
            WHERE
                produtos_x_categorias.id_produto = {$produto['id_produto']} AND
                produtos_x_categorias.ativo = 1 AND 
                categorias.ativo = 1
                ";

            $stmt = $this->conn->prepare($sqlProdutosCategorias);
            $stmt->execute();
            $produtoCategorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (empty($produtoCategorias)) {
                $resultado[$key]["categorias"] = [];
            }else{
                $resultado[$key]["categorias"] = $produtoCategorias;
            }
            
        }

        return $resultado;
    }

    public function buscaUltimosProdutosCadastrados()
    {
        $sql = "SELECT * FROM produtos WHERE ativo = 1 ORDER BY id_produto DESC LIMIT 16";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $produto) {
            $resultado[$key]["nome"] = utf8_encode($produto["nome"]);
            $resultado[$key]["descricao"] = utf8_encode($produto["descricao"]);
            $resultado[$key]["label"] = utf8_encode($produto["nome"]." | ".$produto["sku"]);
            $resultado[$key]["id"] = $produto["id_produto"];

            $sqlProdutosCategorias = "SELECT
                produtos_x_categorias.id_produto_x_categoria,
                categorias.nome AS categoria_nome
            FROM produtos_x_categorias
            LEFT JOIN categorias ON categorias.id_categoria = produtos_x_categorias.id_categoria
            WHERE
                produtos_x_categorias.id_produto = {$produto['id_produto']} AND
                produtos_x_categorias.ativo = 1 AND 
                categorias.ativo = 1
                ";

            $stmt = $this->conn->prepare($sqlProdutosCategorias);
            $stmt->execute();
            $produtoCategorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (empty($produtoCategorias)) {
                $resultado[$key]["categorias"] = [];
            }else{
                $resultado[$key]["categorias"] = $produtoCategorias;
            }
            
        }

        return $resultado;
    }

    public function salvarProduto()
    {
        $sqlNumeroSku = "SELECT * FROM produtos WHERE ativo = 1 AND sku = '{$this->sku}'";
        $stmt = $this->conn->prepare($sqlNumeroSku);
        $stmt->execute();
        $produto = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        if (!empty($produto)) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("Product SKU is already registered!");
            $data["id_produto"] = $produto["id_produto"];
        }else{
            $insertProduto = "INSERT INTO produtos
                            SET
                            nome = '{$this->nome}',
                            sku = '{$this->sku}',
                            preco = {$this->preco},
                            descricao = '{$this->descricao}',
                            quantidade = {$this->quantidade},
                            ativo = {$this->ativo}
            ";
            $stmt = $this->conn->prepare($insertProduto);
            $result = $stmt->execute();
            $id_produto = $this->conn->lastInsertId();

            $data["error"] = 0;
            $data["msg"] = "";
            $data["id_produto"] = $id_produto;

        }
        return $data;
    }

    public function updateProduto()
    {
        $sqlProduto = "SELECT * FROM produtos WHERE ativo = 1 AND id_produto = {$this->id_produto}";
        $stmt = $this->conn->prepare($sqlProduto);
        $stmt->execute();
        $produto = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        if (empty($produto)) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("Product not found!");
            $data["id_produto"] = $this->id_produto;
            
        }else{
            $updateProduto = "UPDATE produtos
                            SET
                                nome = '{$this->nome}',
                                sku = '{$this->sku}',
                                preco = {$this->preco},
                                descricao = '{$this->descricao}',
                                quantidade = {$this->quantidade},
                                ativo = {$this->ativo}
                            WHERE
                                id_produto = {$this->id_produto}
            ";
            $stmt = $this->conn->prepare($updateProduto);
            $result = $stmt->execute();
            $id_produto = $this->id_produto;

            $data["error"] = 0;
            $data["msg"] = "";
            $data["id_produto"] = $id_produto;            
        }

        $data["produto"] = $produto;
        return $data;
    }
    
    public function excluirProduto()
    {
        $sqlProduto = "SELECT * FROM produtos WHERE id_produto = {$this->id_produto}";
        $stmt = $this->conn->prepare($sqlProduto);
        $stmt->execute();
        $produto = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($produto)) {            
            $deleteProduto = "DELETE FROM produtos WHERE id_produto = {$this->id_produto}";
    
            $stmt = $this->conn->prepare($deleteProduto);
            $result = $stmt->execute();
        }

        return $produto;
    }

}
