<?php
namespace classes\dao;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/entity/ProdutoCategoriaEntity.php");

class ProdutoCategoriaDao extends \classes\entity\ProdutoCategoriaEntity {

    public function __construct() {
        parent::__construct();
    }
    
    public function salvarProdutoCategoria()
    {
        $sqlProdutoCategoria = "SELECT * FROM produtos_x_categorias WHERE ativo = 1 AND id_produto = {$this->id_produto} AND id_categoria = {$this->id_categoria}";
        $stmt = $this->conn->prepare($sqlProdutoCategoria);
        $stmt->execute();
        $produtoCategoria = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($produtoCategoria)) {
            $insertProdutoCategoria = "INSERT INTO produtos_x_categorias
                            SET                            
                            id_produto = {$this->id_produto},
                            id_categoria = {$this->id_categoria},
                            ativo = {$this->ativo}
            ";
            
            $stmt = $this->conn->prepare($insertProdutoCategoria);
            $result = $stmt->execute();
            if (empty($result)) {
                $salvo = 0;
            }else{
                $salvo = 1;
            }
        }else{
            $salvo = 1;
        }

        return $salvo;
    }

    public function excluirProdutoCategoriaById()
    {
        $sqlProdutoCategoria = "SELECT * FROM produtos_x_categorias WHERE ativo = 1 AND id_produto_x_categoria = {$this->id_produto_x_categoria}";
        $stmt = $this->conn->prepare($sqlProdutoCategoria);
        $stmt->execute();
        $produtoCategoria = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($produtoCategoria)) {

            $deleteProdutoCategoria = "DELETE FROM produtos_x_categorias WHERE id_produto_x_categoria = {$this->id_produto_x_categoria}";

            $stmt = $this->conn->prepare($deleteProdutoCategoria);
            $result = $stmt->execute();        
            
        }

        return $produtoCategoria;
    }
    
    public function excluirProdutoCategoriaByIdProduto()
    {
        $sqlProdutoCategoria = "SELECT * FROM produtos_x_categorias WHERE ativo = 1 AND id_produto = {$this->id_produto}";
        $stmt = $this->conn->prepare($sqlProdutoCategoria);
        $stmt->execute();
        $produtosCategorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        if (!empty($produtosCategorias)) {
            foreach ($produtosCategorias as $produtoCategoria) {                
                $id_produto_x_categoria = $produtoCategoria["id_produto_x_categoria"];
                $deleteProdutoCategoria = "DELETE FROM produtos_x_categorias WHERE id_produto_x_categoria = {$id_produto_x_categoria}";

                $stmt = $this->conn->prepare($deleteProdutoCategoria);
                $result = $stmt->execute();        
            }
        }

        return $produtosCategorias;
    }

    public function excluirProdutoCategoriaByIdCategoria()
    {
        $sqlProdutoCategoria = "SELECT * FROM produtos_x_categorias WHERE ativo = 1 AND id_categoria = {$this->id_categoria}";
        $stmt = $this->conn->prepare($sqlProdutoCategoria);
        $stmt->execute();
        $produtosCategorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        if (!empty($produtosCategorias)) {
            foreach ($produtosCategorias as $produtoCategoria) {
                $id_produto_x_categoria = $produtoCategoria["id_produto_x_categoria"];
                $deleteProdutoCategoria = "DELETE FROM produtos_x_categorias WHERE id_produto_x_categoria = {$id_produto_x_categoria}";

                $stmt = $this->conn->prepare($deleteProdutoCategoria);
                $result = $stmt->execute();        
            }
        }

        return $produtosCategorias;
    }
    

}