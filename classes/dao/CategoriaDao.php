<?php
namespace classes\dao;
require_once($_SERVER['DOCUMENT_ROOT']."/desafio/classes/entity/CategoriaEntity.php");

class CategoriaDao extends \classes\entity\CategoriaEntity {

    public function __construct() {
        parent::__construct();
    }

    public function buscaCategoriaById()
    {
        $sql = "SELECT * FROM categorias WHERE ativo = 1 AND id_categoria = {$this->id_categoria}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $categorias = $stmt->fetch(\PDO::FETCH_ASSOC);

        $categorias["nome"] = utf8_encode($categorias["nome"]);

        return $categorias;
    }

    public function buscaCategorias()
    {
        $sql = "SELECT * FROM categorias WHERE ativo = 1 ORDER BY id_categoria DESC";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $categorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($categorias as $key => $categoria) {
            $categorias[$key]["nome"] = utf8_encode($categoria["nome"]);
        }

        return $categorias;
    }

    public function buscaCategoriaAutoComplete()
    {
        $sql = "SELECT * FROM categorias WHERE ativo = 1 AND nome LIKE '%{$this->nome}%'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $categorias = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($categorias as $key => $categoria) {
            $categorias[$key]["nome"] = utf8_encode($categoria["nome"]);
            $categorias[$key]["label"] = utf8_encode($categoria["nome"]);
            $categorias[$key]["id"] = $categoria["id_categoria"];
        }

        return $categorias;
    }

    public function salvarCategoria()
    {
        $sqlCodigo = "SELECT * FROM categorias WHERE ativo = 1 AND codigo = '{$this->codigo}'";
        $stmt = $this->conn->prepare($sqlCodigo);
        $stmt->execute();
        $categoria = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        if (!empty($categoria)) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("This Category code is already registered!");
            $data["id_categoria"] = $categoria["id_categoria"];
        }else{
            $insertCategoria = "INSERT INTO categorias
                            SET
                            nome = '{$this->nome}',
                            codigo = '{$this->codigo}',
                            ativo = {$this->ativo}
            ";
            $stmt = $this->conn->prepare($insertCategoria);
            $result = $stmt->execute();
            $id_categoria = $this->conn->lastInsertId();

            $data["error"] = 0;
            $data["msg"] = "";
            $data["id_categoria"] = $id_categoria;

        }
        return $data;
    }

    public function updateCategoria()
    {
        $sqlCategoria = "SELECT * FROM categorias WHERE ativo = 1 AND id_categoria = {$this->id_categoria}";
        $stmt = $this->conn->prepare($sqlCategoria);
        $stmt->execute();
        $categoria = $stmt->fetch(\PDO::FETCH_ASSOC);
        
        if (empty($categoria)) {
            $data["error"] = 1;
            $data["msg"] = utf8_encode("Category not found!");
            $data["id_categoria"] = $this->id_categoria;
        }else{
            $updateCategoria = "UPDATE categorias
                            SET
                                nome = '{$this->nome}',
                                codigo = '{$this->codigo}',
                                ativo = {$this->ativo}
                            WHERE
                                id_categoria = {$this->id_categoria}
            ";
            $stmt = $this->conn->prepare($updateCategoria);
            $result = $stmt->execute();
            $id_categoria = $this->id_categoria;

            $data["error"] = 0;
            $data["msg"] = "";
            $data["id_categoria"] = $id_categoria;

        }
        $data["categoria"] = $categoria;
        return $data;
    }

    public function excluirCategoria()
    {
        $sqlCategoria = "SELECT * FROM categorias WHERE id_categoria = {$this->id_categoria}";
        $stmt = $this->conn->prepare($sqlCategoria);
        $stmt->execute();
        $categoria = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($categoria)) {    
            $deleteCategoria = "DELETE FROM categorias WHERE id_categoria = {$this->id_categoria}";
    
            $stmt = $this->conn->prepare($deleteCategoria);
            $result = $stmt->execute();
        }

        return $categoria;
    }

}