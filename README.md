# Web Jump Assessment

Repositório para o desafio da WebJump

## Requisitos
	- Git
	- PHP 7.3
  	- Docker
  	- Python 3.9
	- Pip
  
## Ferramentas WEB
	- JQuery
  	- JQUery-ui
  	- Bootstrap  	

## Instruções

Utilize o Git para baixar o projeto.

`https://bitbucket.org/kajadu/desafio/src/master/`

## Preparando ambiente da aplicação

#### 1) Abra o terminal e vá até a pasta do docker_file:

cd /desafio/docker_file

#### 2) Rode o comando:

docker build -t web-img .

#### 3) Após terminar de rodar o comando digite:

docker run -v [pasta_do_projeto]:/var/www/html --name WEBJUMP -d -p 8090:80 -p 5010:443 web-img

#### 4) Verifique se o docker startou usando:

docker ps

### Resultado:

| CONTAINER ID | IMAGE | COMMAND | CREATED | STATUS | PORTS | NAMES |
|---|---|---|---|---|---|---|
| cee253f4f7f1 | web-img | "/bin/sh -c '/usr/sb…" |29 hours ago| Up 19 hours | 0.0.0.0:8090->80/tcp, :::8090->80/tcp, 0.0.0.0:5010->443/tcp, :::5010->443/tcp | WEBJUMP |
||||||||


#### 5) Caso não tenha startado rode o seguinte comando:

docker start WEBJUMP

#### 6) Caso queira entrar no docker:

docker exec -it WEBJUMP bash

É necessário ter as seguintes portas livres: 
8090-docker, 5010-docker-ssl

### Acessado o Banco de Dados
 - Link: https://auth-db124.hostinger.com/index.php?db=u547467671_webjump
 - Host: 31.220.104.219
 - User: u547467671_lgjump
 - Password: pWOpCZ|5
  
## Tecnologias utilizadas

|  Tecnologias | Descrição |Repositorio|
|---|---|---|
|Python3| Importar os dados para o Banco de Dados| https://www.python.org/|
|pip| Instalar o pandas para ler o import.csv| apt-get install pip|
|pandas| Ler o import.csv| pip install pandas|
|mysql-connector| Acessar o Banco de Dados pelo Python| pip install mysql-connector|
|   |   ||

